﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject winText;
    public GameObject lossText;
    public GameObject modalBg;

    public void Reset() {        
        this.winText.SetActive(false);
        this.lossText.SetActive(false);
        this.modalBg.SetActive(false);
    }

    public void OnLoose(){        
        this.modalBg.SetActive(true);
        this.winText.SetActive(false);
        this.lossText.SetActive(true);
    }

    public void OnWin() {        
        this.modalBg.SetActive(true);
        this.winText.SetActive(true);
        this.lossText.SetActive(false);
    }
}
