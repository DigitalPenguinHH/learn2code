using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEventEmitter : MonoBehaviour
{
    public GameEvent StartEvent;

    void Start()
    {
        StartEvent.Raise();
    }
}
