using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateTextFromEvent : MonoBehaviour
{
    public void UpdateText(Component sender, object data)
    {
        string textToSet;
        if (data is int @i)
        {
            textToSet = @i.ToString();
        }
        else if (data is IntVariable @iv)
        {
            textToSet = @iv.value.ToString();
        }
        else if (data is string @s)
        {
            textToSet = @s;
        }
        else if (data is StringVariable @sv)
        {
            textToSet = @sv.value;
        }
        else
        {
            Debug.Log("Cannot display text from value : " + data);
            return;
        }
        GetComponent<TMPro.TextMeshProUGUI>().SetText(textToSet);
    }
}
