using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationToUnityEvent : MonoBehaviour
{
    public GameEvent relayEvent;

    public void OnAnimationEvent(AnimationEvent evt)
    {
        relayEvent.Raise();
        Debug.Log("Raised " + relayEvent.name);
    }
}
