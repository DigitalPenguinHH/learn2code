﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenTestShake : MonoBehaviour
{
    public Transform targetPostion;

    public GameObject square;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);

        Tween t = transform.DOMoveX(targetPostion.position.x, 3f, false);
        t.SetAutoKill(false);
        t.OnComplete(() => t.SmoothRewind());
        //.SetLoops(-1, LoopType.Yoyo);
        square.transform.DOShakePosition(3f, 0.2f, 10, 90f, false, false).SetLoops(-1, LoopType.Restart);
    }
}
