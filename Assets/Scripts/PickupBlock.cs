using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBlock : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        ContactPoint2D[] contacts = new ContactPoint2D[collision.contactCount];
        int points = collision.GetContacts(contacts);

        ContactPoint2D cp = contacts[0];
        GameObject other = cp.collider.gameObject;
        if (!other.CompareTag("Player"))
            return;
        //AttachWithFixedJoint(cp, other);
        AttachCangeParent(cp, other);
    }

    private void AttachCangeParent(ContactPoint2D cp, GameObject other)
    {
        this.transform.parent = other.transform;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
    }

    private void AttachWithFixedJoint(ContactPoint2D cp, GameObject other)
    {
        FixedJoint2D joint = this.gameObject.AddComponent<FixedJoint2D>();
        joint.anchor = new Vector2(cp.point.x, cp.point.y);
        //joint.connectedAnchor = new Vector2(cp.point.x, cp.point.y);
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedBody = other.GetComponent<Rigidbody2D>();
        joint.enableCollision = false;
        Debug.Log("Joint Added to " + other);
    }
}
