﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public MapGenerator mapGenerator;

    public Transform playerPosition;
    public Wolf wolf;

    public GameObject ui;
    public GameObject gameOverScreen;

    //
    // Carrots and Events
    //
    public IntVariable carrotsCollected;
    public UnityEvent<IntVariable> onCarrotCollectedEvent;

    void Start()
    {
        carrotsCollected.value = 0;
        gameOverScreen.SetActive(false);
        Wolf.onPlayerCaught += PlayerLooses;
    }

    void Update()
    {
        if (this.gameOverScreen.activeSelf)
            return;

        // Detect Movement
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            playerPosition.position = move(playerPosition, -1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            playerPosition.position = move(playerPosition, 1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            playerPosition.position = move(playerPosition, 0, 1);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            playerPosition.position = move(playerPosition, 0, -1);
        }

        CheckCollision();
        wolf.CheckPlayerCaught();
    }

    private void CheckCollision() 
    {
        GameObject carrotFound = mapGenerator.GetCarrot(playerPosition.position);
        if (carrotFound != null) {
            carrotsCollected.value++;
            mapGenerator.RemoveCarrot(carrotFound);
            onCarrotCollectedEvent.Invoke(this.carrotsCollected);
            if (mapGenerator.GetCarrotsRemaining() == 0) {
                PlayerWins();
            }
        }
    }

    void PlayerWins(){
        wolf.StopMoving();
        gameOverScreen.SetActive(true);
        ui.GetComponent<UIManager>().OnWin();
    }

    void PlayerLooses(){
        wolf.StopMoving();
        GameObject.Find("GameOverScreen").SetActive(true);
        ui.GetComponent<UIManager>().OnLoose();
    }

    private Vector3 move(Transform t, float xDelta, float yDelta)
    {
        Vector3 newPosition = new Vector3(t.position.x + xDelta, t.position.y + yDelta, 0);
        if (mapGenerator.IsOutsideMapBoundary(newPosition)) {
            return t.position;
        } else {
            return newPosition;
        }
    }

    public void OnPlayAgain()
    {
        ui.GetComponent<UIManager>().Reset();
        ui.SetActive(false);
        mapGenerator.Init();
        wolf.StartMoving();
    }
}