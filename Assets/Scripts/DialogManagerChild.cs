﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManagerChild : DialogManager
{
    public override void Greet()
    {
        Debug.Log("Hello from DialogManagerChild");
    }
}
