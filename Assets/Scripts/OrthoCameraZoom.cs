﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class OrthoCameraZoom : MonoBehaviour
{
    public Camera cam;

    [Header("Camera Focus Options")]
    public Transform focusObject;
    public Vector2 cameraOffset = new Vector2(0, 1f);

    [Header("Zoom Options")]
    public float zoomStep = 1f;

    void Awake()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
    }

    void Start()
    {
        
    }

    void Update()
    {
        CalcOffset();
    }

    private void CalcOffset()
    {
        Vector2 lowerCenter = new Vector2(0, cam.orthographicSize * -1) + cameraOffset;
        //Debug.Log(lowerCenter);
        focusObject.position = lowerCenter;
    }

    public void ZoomOut()
    {
        cam.orthographicSize += zoomStep;
    }
}
