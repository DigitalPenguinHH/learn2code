﻿using System;
using System.Collections;
using UnityEngine;

public class FadeObject : MonoBehaviour
{
    private float fadeSpeed = 1.0f;
    public float FadeSpeed { get => fadeSpeed; set => fadeSpeed = value; }

    public void FadeIn()
    {
        StartCoroutine(FadeInObject()); ;
    }

    public void FadeOut()
    {
        StartCoroutine(FadeOutObject());
    }

    public IEnumerator FadeOutObject()
    {
        while (GetComponent<Renderer>().material.color.a > 0)
        {
            UpdateColor((alpha) => alpha - (FadeSpeed * Time.deltaTime));
            yield return null;
        }
    }

    public IEnumerator FadeInObject()
    {
        while (GetComponent<Renderer>().material.color.a < 1)
        {
            UpdateColor((alpha) => alpha + (FadeSpeed * Time.deltaTime));
            yield return null;
        }
    }

    private void UpdateColor(Func<float, float> calcFadeAmount)
    {
        Color objectColor = GetComponent<Renderer>().material.color;
        float fadeAmount = calcFadeAmount.Invoke(objectColor.a);

        objectColor = new Color(objectColor.r, objectColor.r, objectColor.g, fadeAmount);
        GetComponent<Renderer>().material.color = objectColor;
    }
}
