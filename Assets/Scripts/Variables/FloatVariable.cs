using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New FloatVariable", menuName = "Variables/float Variable")]
public class FloatVariable : ScriptableObject
{
    public float value;
}
