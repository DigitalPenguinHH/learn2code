using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New StringVariable", menuName = "Variables/string Variable")]
public class StringVariable : ScriptableObject
{
    public string value;
}
