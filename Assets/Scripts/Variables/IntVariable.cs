using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New IntVariable", menuName = "Variables/int Variable")]
public class IntVariable : ScriptableObject
{
    public int value;
}
