﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    //This script turns down the volume of the crickte ambient sound when the player enters a trigger.
    public class AudioFadeout : MonoBehaviour
    {
        public float fadeDuration = 3f;

        [SerializeField]
        private AudioSource crickets;

        [SerializeField]
        private float currentVolume;

        private void Awake()
        {
            //search for the audiosource that plays the ambient sound, and assigns it to our variable
            crickets = GameObject.Find("AudioManager_Wald").GetComponent<AudioSource>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                FadeOutSound();
            }
        }

        public void FadeOutSound()
        {
            StartCoroutine(VolumeDown());
        }

       private IEnumerator VolumeDown()
        {
            float elapsedTime = 0;
            float startVolume = crickets.volume;

            while (crickets.volume>0)
            {
                elapsedTime += Time.deltaTime;
                crickets.volume = Mathf.Lerp(startVolume, 0, elapsedTime / fadeDuration);
                currentVolume = crickets.volume;

                yield return null;
            }

            crickets.Stop();
        }
    }
}