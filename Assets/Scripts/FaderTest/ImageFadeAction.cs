﻿using System.Collections;
using System;
using UnityEngine;

public abstract class ImageFadeAction : MonoBehaviour
{
    public virtual void Perform()
    {

    }

    public virtual void CleanUp()
    {

    }
}
