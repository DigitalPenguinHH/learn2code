﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageFader : MonoBehaviour
{
    
    private Animator animator;

    public bool resetAfterFadeOut = false;

    public ImageFadeAction onFadeIn;
    public ImageFadeAction afterFadeIn;
    public ImageFadeAction onFadeOut;
    public ImageFadeAction afterFadeOut;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void StartFadeIn()
    {
        animator.SetTrigger("FadeIn");
        onFadeIn?.Perform();
    }

    public void StartFadeOut()
    {
        afterFadeIn?.CleanUp();
        animator.SetTrigger("FadeOut");
        onFadeOut?.Perform();
    }

    public void  AfterFadeIn()
    {
        onFadeIn?.CleanUp();
        afterFadeIn?.Perform();
    }

    public void AfterFadeOut()
    {
        onFadeOut?.CleanUp();
        afterFadeOut?.Perform();

        if (resetAfterFadeOut)
            animator.SetTrigger("Reset");
    }
}
