﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundAction : ImageFadeAction
{

    public AudioSource audioSource;

    public override void Perform()
    {
        audioSource?.Play();
    }
}
