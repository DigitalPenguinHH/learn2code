﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayDialogTextAction : ImageFadeAction
{

    [TextArea(3,6)]
    public string text;
    
    public GameObject textParent;
    public TextMeshProUGUI textUI;


    public override void Perform()
    {
        textUI.text = text;
        textParent.SetActive(true);
    }

    public override void CleanUp()
    {
        textParent.SetActive(false);
    }
}
