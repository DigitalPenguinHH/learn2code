﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class LoadSceneAction : ImageFadeAction
{
    public string sceneName;

    public override void Perform()
    {
        if (sceneName != null)
            SceneManager.LoadScene(sceneName);
    }
}
