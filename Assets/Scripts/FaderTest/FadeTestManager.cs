﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTestManager : MonoBehaviour
{
    public ImageFader imageFader;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.KeypadPlus))
        {
            imageFader.StartFadeIn();
        }
        else if (Input.GetKeyUp(KeyCode.KeypadMinus))
        {
            imageFader.StartFadeOut();
        }
    }
}
