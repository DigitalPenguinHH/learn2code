﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public int MAP_SIZE = 10;
    public Sprite[] floorTilesGrass;
    public GameObject tilePrefab;
    public GameObject carrotPrefab;
    public int carrotCount = 5;
    public GameObject player;
    public GameObject wolf;

    private Sprite[,] map;
    private GameObject mapHolder;
    private Vector3 MAP_OFFSET = new Vector3(-5f, -5f, 0);
    private Vector3 DEFAULT_OFFSET = new Vector3(0.5f, 0.5f, 0);
    private Vector3 WOLF_OFFSET = new Vector3(1.5f, 0, 0);
    private List<GameObject> carrots;
    void Awake()
    {
        Init();
    }

    public void Init()
    {
        this.map = new Sprite[MAP_SIZE,MAP_SIZE];
        this.mapHolder = GameObject.Find("Map");
        this.carrots = new List<GameObject>(carrotCount);
        GenerateFloor();
        GenerateCarrots();
        SetPlayerPosition();
        SetWolfPosition();
    }

    private void GenerateFloor()
    {
        for (int i = 0; i < this.mapHolder.transform.childCount; ++i)
            Destroy(this.mapHolder.transform.GetChild(i).gameObject);
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                map[i,j] = floorTilesGrass[Random.Range(0, floorTilesGrass.Length - 1)];
                Vector3 initPos = GetPositionForTile(i, j);
                tilePrefab.GetComponent<SpriteRenderer>().sprite = map[i,j];
                Instantiate(tilePrefab, initPos, Quaternion.identity, mapHolder.transform);
            }
        }
    }
    private void GenerateCarrots()
    {
        for (int i = 0; i < carrotCount; i++)
        {
            Vector3 randPos;
            do {
                randPos = GetRandomPosition();
            } while(OnSamePosition(randPos, player.transform.position));
            GameObject newCarrot = Instantiate(carrotPrefab, randPos, Quaternion.identity, mapHolder.transform);
            this.carrots.Add(newCarrot);
        }
    }

    public Vector3 GetRandomPosition()
    {
        return GetRandomPosition(0, map.GetLength(0) - 1, 0, map.GetLength(0) - 1);
    }

    public Vector3 GetRandomPosition(int minX, int maxX, int minY, int maxY)
    {
        int randX = Random.Range(minX, maxX);
        int randy = Random.Range(minY, maxY);
        Vector3 randPos = GetPositionForTile(randX, randy);
        return randPos;
    }

    public Vector3 GetPositionForTile(int x, int y)
    {
        Vector3 newPos = new Vector3(x, y, 0) + MAP_OFFSET + DEFAULT_OFFSET;
        return newPos;
    }

    bool OnSamePosition(Vector3 newPos, Vector3 otherPos)
    {
        bool xSame = ((int) newPos.x) == (otherPos.x);
        bool ySame = ((int) newPos.y) == (otherPos.y);
        return xSame && ySame;
    }

    private void SetPlayerPosition()
    {
        Vector3 newPos;
        do {
            newPos = GetRandomPosition();
        } while(this.carrots.FindIndex(o => o.transform.position == newPos) >= 0);
        this.player.transform.position = newPos;
    }

    private void SetWolfPosition()
    {
        Vector3 newPos;
        Vector3 distance;
        do {
            newPos = GetRandomPosition(0, map.GetLength(0) - 2, 1, map.GetLength(0) - 1) + WOLF_OFFSET - DEFAULT_OFFSET;
            distance = newPos - player.transform.position;

        } while(Mathf.Abs(distance.x) < 3 || Mathf.Abs(distance.y) < 3);
        this.wolf.transform.SetPositionAndRotation(newPos, Quaternion.identity);
    }

    public bool IsOutsideMapBoundary(Vector3 position)
    {
        return Mathf.Abs(position.x) > (MAP_SIZE / 2) || Mathf.Abs(position.y) > (MAP_SIZE / 2);
    }

    public GameObject GetCarrot(Vector3 position)
    {
        if (carrots == null || carrots.Count == 0)
            return null;
        foreach(GameObject carrot in carrots)
        {
            if (carrot.transform.position.x == position.x && carrot.transform.position.y == position.y) {
                return carrot;
            }
        }
        return null;
    }

    public void RemoveCarrot(GameObject carrot)
    {
        carrots.Remove(carrot);
        Destroy(carrot);
    }
    
    public int GetCarrotsRemaining()
    {
        return this.carrots.Count;
    }
}
