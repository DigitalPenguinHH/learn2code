﻿using System;

[Serializable]
public class Sentence
{
    public string speaker;
    public string sentence;
}
