﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Highscore
{
    private int score;
    public int Score { get => score; set => score = value; }

    private DateTime time;
    public DateTime Time { get => time; set => time = value; }

}
