﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using TMPro;

class HighScoreComparer : IComparer<Highscore>
{
    public int Compare(Highscore hs1, Highscore hs2)
    {

        if (hs1 == null || hs2 == null)
        {
            return 0;
        }
        return hs1.Score.CompareTo(hs2.Score) * -1;
    }
}

public class HighscoreManager : MonoBehaviour
{

    private List<Highscore> highScores = new List<Highscore>();

    public GameObject highScoreBoard;
    public GameObject linePrefab;

    private string fileName;

    void Awake()
    {
        fileName = Application.persistentDataPath + "/highscores.dat";
        LoadHighScoresFromFile();
    }

    private void LoadHighScoresFromFile()
    {
        if (!File.Exists(fileName))
            return;
        try
        {
            using (Stream stream = File.Open(fileName, FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();
                highScores = bin.Deserialize(stream) as List<Highscore>;
                highScores.Sort(new HighScoreComparer());
            }
            UpdateHighscoreBoard();
        }
        catch (IOException)
        {
            Debug.Log("HighScore loading error.");
        }
    }

    private void UpdateHighscoreBoard()
    {
        foreach (Highscore h in highScores)
        {
            AddLine(h);
        }
    }

    public void Save(Highscore highScore)
    {
        highScores.Add(highScore);
        highScores.Sort(new HighScoreComparer());
        foreach (Transform child in highScoreBoard.transform)
        {
            Destroy(child.gameObject);
        }
        try
        {
            using (Stream stream = File.Open(fileName, FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, highScores);
            }
        }
        catch (IOException)
        {
            Debug.Log("HighScore save error.");
        }
        UpdateHighscoreBoard();
    }

    private void AddLine(Highscore highScore)
    {
        GameObject line = GameObject.Instantiate(linePrefab, highScoreBoard.transform, false);
        line.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = highScore.Time.ToString();
        line.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = highScore.Score.ToString();
    }

    public void NewHighscore()
    {
        Highscore highScore = new Highscore();
        highScore.Score = UnityEngine.Random.Range(1000, 9999);
        highScore.Time = DateTime.Now;
        Save(highScore);
    }

    public void CleaAll()
    {
        highScores.Clear();
        foreach (Transform child in highScoreBoard.transform)
        {
            Destroy(child.gameObject);
        }
    }
}
