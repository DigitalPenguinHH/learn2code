using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerPrefBool : MonoBehaviour
{
    [SerializeField]
    private string name;
    [SerializeField]
    private bool missingToFalse = true;

    [SerializeField]
    UnityEvent ifTrue;
    [SerializeField]
    UnityEvent ifFalse;

    void Awake()
    {
        if (name == null || name.Length == 0)
        {
            Debug.LogError("\"Name\" is missing");
        }
        if (ifTrue == null)
        {
            Debug.LogWarning("No Action for \"true\" state as been set.");
        }
        if (ifFalse == null)
        {
            Debug.LogWarning("No Action for \"false\" state as been set.");
        }
        LoadPref();
    }

    void LoadPref()
    {
        if (PlayerPrefs.HasKey(name))
        {
            bool storedValue = PlayerPrefs.GetInt(name) == 1;
            Debug.Log("Found bool PlayerPref \"" + name + "\" with value: " + storedValue);
            if (storedValue)
            {
                OnTrue();
            }
            else
            {
                OnFalse();
            }
        }
        else if (missingToFalse)
        {
            Debug.Log("No bool PlayerPref \"" + name + "\" found. Defaulting to " + false);
            OnFalse();
        }
    }
    private void OnTrue()
    {
        if (this.ifTrue != null)
        {
            this.ifTrue.Invoke();
        }
    }
    private void OnFalse ()
    {
        if (this.ifFalse != null)
        {
            this.ifFalse.Invoke();
        }
    }
    public void Save(bool value)
    {
        PlayerPrefs.SetInt(name, value ? 1 : 0);
    }
}
