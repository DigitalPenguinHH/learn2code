﻿using System.Collections.Generic;
using UnityEngine;

public class LocalisationSystem
{
    public class Language
    {
        private string name;
        private string id;

        public Language(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        public string Name { get => name; }
        public string Id { get => id; }
    }

    public static Language languageGerman = new Language("German", "de");
    public static Language languageEnglish = new Language("English", "en");

    private static Dictionary<string, string> asdadsa;
}
