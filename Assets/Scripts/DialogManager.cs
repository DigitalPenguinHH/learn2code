﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    public Sentence[] sentences;

    private Queue<Sentence> sentenceQueue;

    private void Awake()
    {
        this.sentenceQueue = new Queue<Sentence>();
    }

    void Start()
    {
        foreach(Sentence f in sentences)
        {
            this.sentenceQueue.Enqueue(f);
        }

        Greet();
    }

    public virtual void Greet()
    {
        Debug.Log("Hello from DialogManager");
    }
}
