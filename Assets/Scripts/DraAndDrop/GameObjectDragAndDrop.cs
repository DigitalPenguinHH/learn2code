﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class GameObjectDragAndDrop : MonoBehaviour
{
    static Color32 COLOR_WHITE = new Color32(255, 255, 255, 255);
    static Color32 COLOR_BLUE = new Color32(126, 146, 255, 168);

    public GameObject[] targetZones;

    // whether the player is currently dragging an item
    private bool draggingItem;
    // allows a grabbed object to stick realistically to the player’s touch position (more about this later).
    private Vector3 touchOffset;
    private Vector3 startPosotion;

    public GameObject draggedObject; 
    
    public float Speed = 10f;

    private bool HasInput
    {
        get
        {
            // returns true if either the mouse button is down or at least one touch is felt on the screen
            return Input.GetMouseButton(0);
        }
    }

    private Vector3 CurrentTouchPosition
    {
        get
        {
            Vector3 inputPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return inputPos;
        }
    }

    void Start()
    {
        this.targetZones = GameObject.FindGameObjectsWithTag("Floe");
    }

    void Update()
    {
        if (HasInput)
        {
            DragOrPickUp();
        }
        else
        {
            if (draggingItem)
                DropItem();
        }
    }

    private void DragOrPickUp()
    {
        Vector3 inputPosition = CurrentTouchPosition;
        if (draggingItem)
        {
            draggedObject.transform.position = inputPosition + touchOffset;
        }
        else
        {
            RaycastHit2D[] touches = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
            touches = touches.Where(t => t.transform.gameObject.tag != "Floe").ToArray();
            if (touches.Length > 0)
            {
                RaycastHit2D hit = touches[0];
                if (hit.transform != null)
                {
                    draggingItem = true;
                    draggedObject = hit.transform.gameObject;
                    Debug.Log("Picked up: " + hit.transform.gameObject.name);
                    touchOffset = (Vector3)hit.transform.position - inputPosition;
                    startPosotion = draggedObject.transform.localPosition;
                }
            }
        }
    }
    private void DropItem()
    {
        draggingItem = false;

        // check if is in drop zone
        for (int i = 0; i < targetZones.Length; i++)
        {
            SpriteRenderer renderer = targetZones[i].GetComponent<SpriteRenderer>();
            if (renderer.bounds.Intersects(draggedObject.GetComponent<SpriteRenderer>().bounds))
            {
                GameObject onIceFloeCurrent = draggedObject.GetComponent<Pengwin>().icefloe;

                Debug.Log("Dropped on " + targetZones[i].name);
                renderer.color = COLOR_BLUE;

                if (onIceFloeCurrent != null)
                {
                    Debug.Log("Pengwin is currently on " + onIceFloeCurrent);
                    if (onIceFloeCurrent != targetZones[i])
                    {
                        onIceFloeCurrent.GetComponent<Icefloe>().Remove(draggedObject.GetComponent<Pengwin>());
                    }
                    if (!onIceFloeCurrent.GetComponent<Icefloe>().IsOccupied())
                    {
                        onIceFloeCurrent.GetComponent<SpriteRenderer>().color = COLOR_WHITE;
                    }
                }

                targetZones[i].GetComponent<Icefloe>().OccupyWith(draggedObject.GetComponent<Pengwin>());
                draggedObject.GetComponent<Pengwin>().icefloe = targetZones[i];

                return;
            }
        }

        // Revert position
        StartCoroutine(ReturnToNormalPos());
        draggingItem = false;
    }

    private IEnumerator ReturnToNormalPos()
    {
        while (draggedObject.transform.localPosition != startPosotion)
        {
            draggedObject.transform.localPosition = Vector3.Lerp(draggedObject.transform.localPosition, startPosotion, Speed * Time.deltaTime);
            yield return null;
        }
    }

}
