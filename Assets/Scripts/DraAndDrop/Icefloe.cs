using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icefloe : MonoBehaviour
{
    [SerializeField]
    private List<Pengwin> occupants = new List<Pengwin>(5);

    internal bool IsOccupied()
    {
        return this.occupants != null && this.occupants.Count > 0;
    }

    internal void OccupyWith(Pengwin pengwin)
    {
        if (!occupants.Contains(pengwin))
        {
            occupants.Add(pengwin);
        }
    }

    internal void Remove(Pengwin pengwin)
    {
        occupants.Remove(pengwin);
    }
}
