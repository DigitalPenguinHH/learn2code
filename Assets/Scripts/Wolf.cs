﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{
    public delegate void OnPlayerCaught();
    public static event OnPlayerCaught onPlayerCaught;

    public GameObject player;
    public int moveIntervalSeconds = 3;

    private MapGenerator mapGenerator;

    void Start()
    {
        this.player = GameObject.Find("Player");
        this.mapGenerator = GameObject.Find("MapGenerator").GetComponent<MapGenerator>();
        StartMoving();
    }

    public void StartMoving()
    {        
        InvokeRepeating("MoveTowardsPlayer", this.moveIntervalSeconds, this.moveIntervalSeconds);
    }

    void MoveTowardsPlayer()
    {
        int moveX = 0;
        int moveY = 0;
        if (player.transform.position.x < this.transform.position.x) {
            moveX = -1;
            this.transform.localScale.Set(1, this.transform.localScale.y, 1);
            this.transform.localScale = new Vector3(1,transform.localScale.y,1);
        } else if (player.transform.position.x > this.transform.position.x) {
            moveX = 1;
            this.transform.localScale.Set(-1, this.transform.localScale.y, 1);
            this.transform.localScale = new Vector3(-1,transform.localScale.y,1);
        }
        if (player.transform.position.y < this.transform.position.y) {
            moveY = -1;
        } else if (player.transform.position.y > this.transform.position.y) {
            moveY = 1;
        }
        Vector3 movement = new Vector3(moveX, moveY, 0);
        this.transform.position = this.transform.position + movement;
        CheckPlayerCaught();
    }

    public void CheckPlayerCaught()
    {
        if (this.GetComponent<BoxCollider2D>().bounds.Intersects(player.GetComponent<BoxCollider2D>().bounds)) {
            Debug.Log("Caught the player!");
            onPlayerCaught();
        }
    }

    public void StopMoving() {        
        CancelInvoke();
    }
}
